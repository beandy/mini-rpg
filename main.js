/* START VAR AND FUNCTION ABOUT HERO */

// valeurs du hero
var hero = [
// [0] : current_HealthPoints
1000,

// [1] : max_HealthPoints
1000,

// [2] : current_ShieldPoints
1000,

// [3] : max_ShieldPoints
1000,

// [4] : current_StaminaPoints
1000,

// [5] : max_StaminaPoints
1000,

// [6] : global_coefficient
1,

// [7] : strength_coefficient
1,

// [8] : drugology_coefficient
1,
]

// inserer les valeurs du hero dans les stats
function insertHeroValues() {
	document.getElementById('current-health-points').innerHTML = hero[0]
	document.getElementById('max-health-points').innerHTML = hero[1]

	document.getElementById('current-shield-points').innerHTML = hero[2]
	document.getElementById('max-shield-points').innerHTML = hero[3]

	document.getElementById('current-stamina-points').innerHTML = hero[4]
	document.getElementById('max-stamina-points').innerHTML = hero[5]
	document.getElementById('strength-level').innerHTML = hero[7]
	document.getElementById('drugology-level').innerHTML = hero[8]
}

// initialiser insertion des valeurs du hero au demarrage
insertHeroValues()

/* END VAR AND FUNCTION ABOUT HERO */

/* START VAR AND FUNCTION ABOUT ITEMS */

// valeurs de l'item pour la potion de santé
var healthPotion = [
// [0] : Name
"Health potion",

// [1] : Gain
100,

// [2] : Much_Avaible
2,
]
// descriptif de la potion de santé
var descriptifHealthPotion = "This potion will restore " + healthPotion[1] + " points to your health."

// valeurs de l'item pour la potion de défense
var shieldPotion = [
// [0] : Name
"Shield potion",

// [1] : Gain
100,

// [2] : Much_Avaible
2,
]
// descriptif de la potion d'armure
var descriptifShieldPotion = "This potion will restore " + shieldPotion[1] + " points to your shield."

// valeurs de l'item pour la potion d'endurance
var staminaPotion = [
// [0] : Name
"Stamina potion",

// [1] : Gain
100,

// [2] : Much_Avaible
2,
]
// descriptif de la potion d'armure
var descriptifStaminaPotion = "This potion will restore " + staminaPotion[1] + " points to your stamina."

// valeurs de l'item pour la potion de boost
var boostPotion = [
// [0] : Name
"Boost potion",

// [1] : Gain
,

// [2] : Much_Avaible
0,
]
// descriptif de la potion de boost
var descriptifBoostPotion = "During a whole fight, this potion will increase your strength when using weapons and the effectiveness of others potions."

// valeurs de l'item pour le draineur d'energie
var energyDrain = [
// [0] : Name
"Energy drain",

// [1] : Gain
2,

// [2] : Much_Avaible
0,
]
// descriptif du draineur d'energie
var descriptifEnergyDrain = "This energy drain reduce the health\'s monster you are facing by half."

// liste des items qui existe dans le jeu
var itemsExisting = [healthPotion, shieldPotion, staminaPotion, boostPotion, energyDrain]

// liste des items actuellement dans l'inventaire du héros
var itemsOwned = [healthPotion, shieldPotion, staminaPotion]

// créer l'élément HTML d'un item dans l'inventaire
function printItemInList(itemId, name, much, id_much, id_info_btn, id_use_btn, infoFunction, useFunction) {
	document.getElementById('list-items').innerHTML += '<tr id="' + itemId + '">' + '<td>' + name + '</td>' + '<td class="nes-text">' + 'x' + '<span id="' + id_much + '">' + much + '</span>' + '</td>' + '<td>' + '<button class="nes-btn" ' + 'id="' + id_info_btn + '">' + 'info' + '</button>' + '</td>' + '<td>' + '<button class="nes-btn" '+ 'id="' + id_use_btn + '">' + 'use' + '</button>' + '</td>' + '</tr>'

	var infoBtn = document.getElementById(id_info_btn)
	infoBtn.setAttribute("onclick", infoFunction)

	var useBtn = document.getElementById(id_use_btn)
	useBtn.setAttribute("onclick", useFunction)
}

// imprimer dans l'inventaire un item
function printItemsOwned(item) {
	switch (item) {
		case healthPotion :
			printItemInList("health-potion-item", healthPotion[0], healthPotion[2], "much-health-potion", "info-health-potion", "use-health-potion", 'printItemInfo(healthPotion[0], descriptifHealthPotion)', 'useHealthPotion()');
			break;
		case shieldPotion :
			printItemInList("shield-potion-item", shieldPotion[0], shieldPotion[2], "much-shield-potion", "info-shield-potion", "use-shield-potion", 'printItemInfo(shieldPotion[0], descriptifShieldPotion)', 'useShieldPotion()');
			break;
		case staminaPotion :
			printItemInList("stamina-potion-item", staminaPotion[0], staminaPotion[2], "much-stamina-potion", "info-stamina-potion", "use-stamina-potion", 'printItemInfo(staminaPotion[0], descriptifStaminaPotion)', 'useStaminaPotion()');
			break;
		case boostPotion :
				printItemInList("boost-potion-item", boostPotion[0], boostPotion[2], "much-boost-potion", "info-boost-potion", "use-boost-potion", 'printItemInfo(boostPotion[0], descriptifBoostPotion)', 'useBoostPotion()');
			break;
		case energyDrain :
				printItemInList("energy-drain-item",  energyDrain[0], energyDrain[2], "much-energy-drain", "info-energy-drain", "use-energy-drain", 'printItemInfo(energyDrain[0], descriptifEnergyDrain)', 'useEnergyDrain()');
			break;
	}
}

// fonction pour faire apparaître le panel de la liste des items
function offDefaultPanelItems() {
	document.getElementById('sub-panel-items-default').classList.toggle('hide')
	document.getElementById('sub-panel-items-list').classList.toggle('hide')
}

// réinitialiser à l'affichage le décompte des items dans l'inventaire
function refreshMuchItems() {
	for (i = 0; i < itemsOwned.length; i++) {
		switch (itemsOwned[i]) {
			case healthPotion :
				document.getElementById('much-health-potion').innerHTML = healthPotion[2]
				break;
			case shieldPotion :
				document.getElementById('much-shield-potion').innerHTML = shieldPotion[2]
				break;
			case staminaPotion :
				document.getElementById('much-stamina-potion').innerHTML = staminaPotion[2]
				break;
			case boostPotion :
				document.getElementById('much-boost-potion').innerHTML = boostPotion[2]
				break;
			case energyDrain :
				document.getElementById('much-energy-drain').innerHTML = energyDrain[2]
				break;
		}
	}
}

// effet et interaction de l'usage d'une potion de santé
function useHealthPotion() {
	// if the health is not already at max AND potion is avaible
	if (hero[0] < hero[1] && healthPotion[2] > 0) {
		// increase the current health points with the potion gain
		hero[0] = Math.round(hero[0] + (healthPotion[1] * (hero[6]*hero[8])))
		// if the new current health points are higher than max, then equalize the current health points
		if (hero[0] > hero[1]) {
			hero[0] = hero[1]
		}
		// refresh hero's stats
		insertHeroValues()
		// decrease the number of potion avaible
		healthPotion[2] = healthPotion[2] - 1
		// refresh item's avaibility
		refreshMuchItems()
		// make the health potion unusable if no more available
		if (healthPotion[2] <= 0) {
			// rendre disable le bouton
			document.getElementById('use-health-potion').disabled = true
			document.getElementById('use-health-potion').classList.add('is-disabled')
			// colorer le compte de potion a 0
			document.getElementById('much-health-potion').parentNode.classList.add('is-error')
		}
	}
	// if the health is already full
	else if (hero[0] >= hero[1]) {
		document.getElementById('alert-stats').classList.replace('hide', 'show')
		document.getElementById('alert-stats').innerHTML = 'Your ' + '<span class="nes-text is-primary">health</span>' + ' is already full.'
		setTimeout(function() {
			document.getElementById('alert-stats').classList.replace('show', 'hide')
		}, 1000)
	} 
}

function useShieldPotion() {
	// if the shield is not already at max AND potion is avaible
	if (hero[2] < hero[3] && shieldPotion[2] > 0) {
		// increase the current shield points with the potion gain
		hero[2] = Math.round(hero[2] + (shieldPotion[1] * (hero[6]*hero[8])))
		// if the new current shield points are higher than max, then equalize the current shield points
		if (hero[2] > hero[3]) {
			hero[2] = hero[3]
		}
		// refresh hero's stats
		insertHeroValues()
		// decrease the number of potion avaible
		shieldPotion[2] = shieldPotion[2] - 1
		// refresh item's avaibility
		refreshMuchItems()
		// make the shield potion unusable if no more available
		if (shieldPotion[2] <= 0) {
			// desactiver le bouton
			document.getElementById('use-shield-potion').disabled = true
			document.getElementById('use-shield-potion').classList.add('is-disabled')
			// colorer le compte de potion a 0
			document.getElementById('much-shield-potion').parentNode.classList.add('is-error')
		}
	}
	// if the shield is already full
	else if (hero[2] >= hero[3]) {
		document.getElementById('alert-stats').classList.replace('hide', 'show')
		document.getElementById('alert-stats').innerHTML = 'Your ' + '<span class="nes-text is-primary">shield</span>' + ' is already full.'
		setTimeout(function() {
			document.getElementById('alert-stats').classList.replace('show', 'hide')
		}, 1000)
	} 
}

function useStaminaPotion() {
	// if the stamina is not already at max AND potion is avaible
	if (hero[4] < hero[5] && staminaPotion[2] > 0) {
		// increase the current stamina points with the potion gain
		hero[4] = Math.round(hero[4] + (staminaPotion[1] * (hero[6]*hero[8])))
		// if the new current stamina points are higher than max, then equalize the current stamina points
		if (hero[4] > hero[5]) {
			hero[4] = hero[5]
		}
		// refresh hero's stats
		insertHeroValues()
		// decrease the number of potion avaible
		staminaPotion[2] = staminaPotion[2] - 1
		// refresh item's avaibility
		refreshMuchItems()
		// make the stamina potion unusable if no more available
		if (staminaPotion[2] <= 0) {
			// desactiver le bouton
			document.getElementById('use-stamina-potion').disabled = true
			document.getElementById('use-stamina-potion').classList.add('is-disabled')
			// colorer le compte de potion a 0
			document.getElementById('much-stamina-potion').parentNode.classList.add('is-error')
		}
	}
	// if the stamina is already full
	else if (hero[4] >= hero[5]) {
		document.getElementById('alert-stats').classList.replace('hide', 'show')
		document.getElementById('alert-stats').innerHTML = 'Your ' + '<span class="nes-text is-primary">stamina</span>' + ' is already full.'
		setTimeout(function() {
			document.getElementById('alert-stats').classList.replace('show', 'hide')
		}, 1000)
	} 
}

function useBoostPotion() {
	hero[6] = hero[6] + (hero[6]/2)
	// decrease the number of potion avaible
	boostPotion[2] = boostPotion[2] - 1
	// refresh item's avaibility
	refreshMuchItems()
	// make the boost potion unusable if no more available
	if (boostPotion[2] <= 0) {
		// desactiver le bouton
		document.getElementById('use-boost-potion').disabled = true
		document.getElementById('use-boost-potion').classList.add('is-disabled')
		// colorer le compte de potion a 0
		document.getElementById('much-boost-potion').parentNode.classList.add('is-error')
	}
}

function useEnergyDrain() {
	// allow the use only if the hero is currently fighting
	if (fighting == 1) {
		// decrease monster's health
		currentMonster[2] = currentMonster[2] - (currentMonster[2]/(energyDrain[1]*hero[6]))
		// update the progress bar
		document.getElementById('monster-stats').value = currentMonster[2]
		// decrease the number of energy drain avaible
		energyDrain[2] = energyDrain[2] - 1
		// refresh item's avaibility
		refreshMuchItems()
		// make the energy drain unusable if no more available
		if (energyDrain[2] <= 0) {
			// desactiver le bouton
			document.getElementById('use-energy-drain').disabled = true
			document.getElementById('use-energy-drain').classList.add('is-disabled')
			// colorer le compte de potion a 0
			document.getElementById('much-energy-drain').parentNode.classList.add('is-error')
		}
	}
}

/* END VAR AND FUNCTION ABOUT ITEMS */

/* START SWITCH BETWEEN PANELS */

document.getElementById('nav-weapons').onclick = function() {
	if (!document.getElementById('nav-weapons').classList.contains('nav-selected')) {
		document.getElementById('nav-weapons').classList.add('nav-selected')
		
		document.getElementById('nav-items').classList.remove('nav-selected')

		document.getElementById('panel-weapons').classList.replace('hide', 'show')

		document.getElementById('panel-items').classList.replace('show', 'hide')
	}
}

document.getElementById('nav-items').onclick = function() {
	if (!document.getElementById('nav-items').classList.contains('nav-selected')) {
		document.getElementById('nav-items').classList.add('nav-selected')
		
		document.getElementById('nav-weapons').classList.remove('nav-selected')

		document.getElementById('panel-items').classList.replace('hide', 'show')

		document.getElementById('panel-weapons').classList.replace('show', 'hide')
	}
}

/* END SWITCH BETWEEN PANELS */

/* START DISPLAY INFO SUB-PANELS */

// Insérer le contenu pour le descriptif d'un item
function printItemInfo(name, descriptif) {
	document.getElementById('title-item-info').innerHTML = 'info:' + '<em>' + name + '</em>'
	document.getElementById('text-item-info').innerHTML = descriptif
	toggleDisplay_itemsPanels()
}

// toggle l'affichage du panneau d'info à la place de la liste des items
function toggleDisplay_itemsPanels() {
	document.getElementById('sub-panel-items-infos').classList.toggle('hide')
	document.getElementById('sub-panel-items-list').classList.toggle('hide')
}

// toggle l'affichage du panneau de la liste à la place des infos des items
document.getElementById('back-items').onclick = function() {
	document.getElementById('sub-panel-items-infos').classList.toggle('hide')
	document.getElementById('sub-panel-items-list').classList.toggle('hide')
}


// Insérer le contenu pour le descriptif d'une weapon
function printWeaponInfo(name, descriptif) {
	document.getElementById('title-weapon-info').innerHTML = 'info:' + '<em>' + name + '</em>'
	document.getElementById('text-weapon-info').innerHTML = descriptif
	toggleDisplay_weaponsPanels()
}

// toggle l'affichage du panneau d'info à la place de la liste des weapons
function toggleDisplay_weaponsPanels() {
	document.getElementById('sub-panel-weapons-infos').classList.toggle('hide')
	document.getElementById('sub-panel-weapons-list').classList.toggle('hide')
}

// toggle l'affichage du panneau de la liste à la place des infos des items
document.getElementById('back-weapons').onclick = function() {
	document.getElementById('sub-panel-weapons-infos').classList.toggle('hide')
	document.getElementById('sub-panel-weapons-list').classList.toggle('hide')
}

/* END DISPLAY INFO SUB-PANELS */

/* START VAR ABOUT WEAPONS */

var woodstickWeapon = [
// [0] : Name
"Wood stick",

// [1] : AttackValue
100,

// [2] : AttackingCost
75,
]
// descriptif de la potion d'armure
var descriptifWoodstickWeapon = "A simple stick of wood.<br/><br/>Costs " + woodstickWeapon[2] + " points of stamina."

var steelaxeWeapon = [
// [0] : Name
"Steel axe",

// [1] : AttackValue
200,

// [2] : AttackingCost
150,
]
// descriptif de la potion d'armure
var descriptifSteelaxeWeapon = "A simple axe of steel.<br/><br/>Costs " + steelaxeWeapon[2] + " points of stamina."

var machinegunWeapon = [
// [0] : Name
"Machine-gun",

// [1] : AttackValue
300,

// [2] : AttackingCost
225,
]
// descriptif de la potion d'armure
var descriptifMachinegunWeapon = "A greasy machine-gun fully loaded.<br/><br/>Costs " + machinegunWeapon[2] + " points of stamina."

// liste des armes qui existe dans le jeu
var weaponsExisting = [woodstickWeapon, steelaxeWeapon, machinegunWeapon]

// liste des armes actuellement dans l'inventaire du héros
var weaponsOwned = [woodstickWeapon]

// weapon currently equiped
var weaponEquiped = woodstickWeapon

// créer l'élément HTML d'un item dans l'inventaire
function printWeaponInList(weaponId, name, attackValue, id_info_btn, id_equip_btn, infoFunction, equipFunction) {
	document.getElementById('list-weapons').innerHTML += '<tr id="' + weaponId + '">' + '<td class="nes-text">' + name + '</td>' + '<td>' + attackValue + ' ATK</td>' + '<td>' + '<button class="nes-btn" ' + 'id="' + id_info_btn + '">' + 'info' + '</button>' + '</td>' + '<td>' + '<button class="nes-btn" '+ 'id="' + id_equip_btn + '">' + 'equip' + '</button>' + '</td>' + '</tr>'

	var infoBtn = document.getElementById(id_info_btn)
	infoBtn.setAttribute("onclick", infoFunction)

	var equipBtn = document.getElementById(id_equip_btn)
	equipBtn.setAttribute("onclick", equipFunction)
}

// imprimer dans l'inventaire les items disponible
function printWeaponsOwned(weapon) {
	switch (weapon) {
		case woodstickWeapon :
			printWeaponInList("woodstick-weapon", woodstickWeapon[0], woodstickWeapon[1], "info-woodstick-weapon", "equip-woodstick-weapon", 'printWeaponInfo(woodstickWeapon[0], descriptifWoodstickWeapon)', 'equipWoodstickWeapon()');
			break;
		case steelaxeWeapon :
			printWeaponInList("steelaxe-weapon", steelaxeWeapon[0], steelaxeWeapon[1], "info-steelaxe-weapon", "equip-steelaxe-weapon", 'printWeaponInfo(steelaxeWeapon[0], descriptifSteelaxeWeapon)', 'equipSteelaxeWeapon()');
			break;
		case machinegunWeapon :
			printWeaponInList("machinegun-weapon", machinegunWeapon[0], machinegunWeapon[1], "info-machinegun-weapon", "equip-machinegun-weapon", 'printWeaponInfo(machinegunWeapon[0], descriptifMachinegunWeapon)', 'equipMachinegunWeapon()');
			break;
	}
}

// fonction pour faire apparaître le panel de la liste des weapons
function offDefaultPanelWeapons() {
	document.getElementById('sub-panel-weapons-default').classList.toggle('hide')
	document.getElementById('sub-panel-weapons-list').classList.toggle('hide')
}

function equipWoodstickWeapon() {
	weaponEquiped = woodstickWeapon
	document.getElementById('weapon-equiped').innerHTML = weaponEquiped[0]
}

function equipSteelaxeWeapon() {
	weaponEquiped = steelaxeWeapon
	document.getElementById('weapon-equiped').innerHTML = weaponEquiped[0]
}

function equipMachinegunWeapon() {
	weaponEquiped = machinegunWeapon
	document.getElementById('weapon-equiped').innerHTML = weaponEquiped[0]
}

/* END VAR ABOUT WEAPONS */

/* START VAR ABOUT MONSTERS */

var monsterLVL1 = [
// [0] : Name
"LVL1",

// [1] : AttackValue
100,

// [2] : Current_Health
100,

// [3] : Max_Health
100,

// [4] : Coefficient
1,
]

var monsterLVL2 = [
// [0] : Name
"LVL2",

// [1] : AttackValue
150,

// [2] : Current_Health
150,

// [3] : Max_Health
150,

// [4] : Coefficient
1,
]

var monsterLVL3 = [
// [0] : Name
"LVL3",

// [1] : AttackValue
200,

// [2] : Current_Health
200,

// [3] : Max_Health
200,

// [4] : Coefficient
1,
]

var monsterLVL4 = [
// [0] : Name
"LVL4",

// [1] : AttackValue
250,

// [2] : Current_Health
250,

// [3] : Max_Health
250,

// [4] : Coefficient
1,
]

var monsterLVL5 = [
// [0] : Name
"LVL5",

// [1] : AttackValue
300,

// [2] : Current_Health
300,

// [3] : Max_Health
300,

// [4] : Coefficient
1,
]

var monsterLVL6 = [
// [0] : Name
"LVL6",

// [1] : AttackValue
350,

// [2] : Current_Health
350,

// [3] : Max_Health
350,

// [4] : Coefficient
1,
]

var monsterLVL7 = [
// [0] : Name
"LVL7",

// [1] : AttackValue
400,

// [2] : Current_Health
400,

// [3] : Max_Health
400,

// [4] : Coefficient
1,
]

var monsterLVL8 = [
// [0] : Name
"LVL8",

// [1] : AttackValue
450,

// [2] : Current_Health
450,

// [3] : Max_Health
450,

// [4] : Coefficient
1,
]

var monsterLVL9 = [
// [0] : Name
"LVL9",

// [1] : AttackValue
500,

// [2] : Current_Health
500,

// [3] : Max_Health
500,

// [4] : Coefficient
1,
]

var monsterLVL10 = [
// [0] : Name
"LVL10",

// [1] : AttackValue
550,

// [2] : Current_Health
550,

// [3] : Max_Health
550,

// [4] : Coefficient
1,
]

var monsterLVL11 = [
// [0] : Name
"LVL11",

// [1] : AttackValue
600,

// [2] : Current_Health
600,

// [3] : Max_Health
600,

// [4] : Coefficient
1,
]

var existingMonsters = [monsterLVL1, monsterLVL2, monsterLVL3, monsterLVL4, monsterLVL5, monsterLVL6, monsterLVL7, monsterLVL8, monsterLVL9, monsterLVL10, monsterLVL11]

/* END VAR ABOUT MONSTERS */

/* START VAR AND FUNCTION FOR ENCOUNTER BETWEEN HERO AND A MONSTER */

// monster encoutered in the current room
var currentMonster = monsterLVL1

function heroAttacking() {
	// decrease the stamina
	hero[4] = hero[4] - weaponEquiped[2]
	// decrease monster's health
	currentMonster[2] = currentMonster[2] - (weaponEquiped[1] * (hero[6]*hero[7]))
	// update the progress bar
	document.getElementById('monster-stats').value = currentMonster[2]
	
	if (currentMonster[2] <= 0) {
		// remove the battle scene
		document.getElementById('battle-scene').parentNode.removeChild(document.getElementById('battle-scene'))
		currentMonster[2] = currentMonster[3]
		// reset the hero's coefficient
		hero[6] = 1
		// so the monster is dead then the fight is finished
		fighting = 0
		// displayin the message sayin that the monster was defeated
		msgInFeed(
			'<p>' + 'You defeated the monster !' + '</p>' + '<br/>' + '<button class="nes-btn" onclick="endingRoom()">Ok</button>'
			)
		autofocusMainAction()
	} else {
		monsterAttacking()
	}
}

// contre-attaque du monstre sur le héros
function monsterAttacking() {
	// currentHeroShield - (monsterAttackValue * monsterCoefficient)
	var dmgToHero = hero[2] - (currentMonster[1] * currentMonster[4])
	if (dmgToHero >= 0) {
		hero[2] = dmgToHero
	} else {
		hero[2] = 0
		hero[0] = hero[0] - Math.abs(dmgToHero)
	}
}

function heroIsDead() {
	if (hero[0] <= 0) {
		// remettre à 0 la santé du hero pour éviter l'affichage de valeur négative
		hero[0] = 0
		// réinitialiser le coefficient du hero
		hero[6] = 1
		// le hero est mort alors le combat est terminé
		fighting = 0
		// afficher le message disant que le héros est mort
		txtInFeed('You are dead lil bitch !' + '<br/>' + '<button class="nes-btn" onclick="restartGame()">Restart</button>')
	}
}

function heroGiveUp() {
	// afficher le message disant que le héros est mort
		txtInFeed('Well you died lil bitch !' + '<br/><br/>' + '<button class="nes-btn" onclick="restartGame()">Restart</button>')
}

function triggerHeroAttack() {
	if (hero[4] >= weaponEquiped[2]) {
		heroAttacking()
		insertHeroValues()
		heroIsDead()
		insertHeroValues()
		document.getElementById('alert-stamina').classList.replace('show', 'hide')
	} else {
		document.getElementById('alert-stamina').classList.replace('hide', 'show')
		document.getElementById('alert-stamina').innerHTML = '<br/>' + '<p>' + 'Not enough stamina to use this weapon : ' + '<em>' + weaponEquiped[0] + '</em>.' + '<br/>' + 'Take a ' + '<em>' + staminaPotion[0] + '</em>' + ' or equip yourself with another weapon.' + '</p>' + '<p>' + '<a href="#" onclick="heroGiveUp()">Let me die please ...</a>' + '</p>'
	}
}

/* END VAR AND FUNCTION FOR ENCOUNTER BETWEEN HERO AND A MONSTER */

/* START INTRODUCTION'S SPEECH */


// message #0 de la conversation d'introduction
msgInFeed(
	'<p class="title"><span class="is-old-man">Creepy old man</span></p>'
	+
	'<p>' + 'Hello' + ' <span class="is-hero"><em>Hero</em></span>, ' + 'and welcome to this terrible hood !' + '</p>'
	+
	'<br/>' + '<button class="nes-btn" onclick="introContinueTo1()">Continue</button>'
		)
autofocusMainAction()
	
// message #1 de la conversation d'introduction
function introContinueTo1() {
	msgInFeed(
		'<p class="title"><span class="is-hero">You</span></p>'
		+
		'<p>Where am I ?</p>'
		+
		'<br/>' + '<button class="nes-btn" onclick="introContinueTo2()">Continue</button>'
		)
	autofocusMainAction()
}

// message #2 de la conversation d'introduction
function introContinueTo2() {
	msgInFeed(
		'<p class="title"><span class="is-old-man">Creepy old man</span></p>'
		+
		'<p>SHUT THE FUCK UP !!!</p>'
		+
		'<br/>' + '<button class="nes-btn" onclick="introContinueTo3()">Continue</button>'
		)
	autofocusMainAction()
}

// message #3 de la conversation d'introduction
function introContinueTo3() {
	msgInFeed(
		'<p class="title"><span class="is-old-man">Creepy old man</span></p>'
		+
		'<p><em>"Where am I ?"</em> gneugneugneu... WHO CARES !!?</p>'
		+
		'<br/>' + '<button class="nes-btn" onclick="introContinueTo4()">Continue</button>'
		)
	autofocusMainAction()
}

// message #4 de la conversation d'introduction
function introContinueTo4() {
	msgInFeed(
		'<p class="title"><span class="is-old-man">Creepy old man</span></p>'
		+
		'<p>Do you see the tower there ? You will get in there and reach the rooftop !<br/><br/>But be careful, the building is full of monsters...</p>'
		+
		'<br/>' + '<button class="nes-btn" onclick="introContinueTo5()">Continue</button>'
		)
	autofocusMainAction()
}

// message #5 de la conversation d'introduction
function introContinueTo5() {
	msgInFeed(
		'<p class="title"><span class="is-hero">You</span></p>'
		+
		'<p>WHAT ? WHY ?<br/><br/>NO WAY I\'LL NOT DO THAT !!</p>'
		+
		'<br/>' + '<button class="nes-btn" onclick="introContinueTo6()">Continue</button>'
		)
	autofocusMainAction()
}

// message #6 de la conversation d'introduction
function introContinueTo6() {
	msgInFeed(
		'<p class="title"><span class="is-old-man">Creepy old man</span></p>'
		+
		'<p><em>*sigh...*</em></p>'
		+
		'<br/>' + '<button class="nes-btn" onclick="introContinueTo7()">Continue</button>'
		)
	autofocusMainAction()
}

// message #7 de la conversation d'introduction
function introContinueTo7() {
	msgInFeed(
		'<p>The <span class="is-old-man"><em>Creepy old man</em></span> smacked you (heavily)</p>'
		+
		'<br/>' + '<button class="nes-btn" onclick="introContinueTo8()">Continue</button>'
		)
	autofocusMainAction()
}

// message #8 de la conversation d'introduction
function introContinueTo8() {
	msgInFeed(
		'<p class="title"><span class="is-old-man">Creepy old man</span></p>'
		+
		'<p>Get this tear up ! Stay focus and listen to me.</p>'
		+
		'<br/>' + '<button class="nes-btn" onclick="introContinueTo9()">Continue</button>'
		)
	autofocusMainAction()
}

// message #9 de la conversation d'introduction
function introContinueTo9() {
	msgInFeed(
		'<p class="title"><span class="is-hero">You</span></p>'
		+
		'<p><em>*Sniff...*</em></p>'
		+
		'<br/>' + '<button class="nes-btn" onclick="introContinueTo10()">Continue</button>'
		)
	autofocusMainAction()
}

// message #10 de la conversation d'introduction
function introContinueTo10() {
	msgInFeed(
		'<p class="title"><span class="is-old-man">Creepy old man</span></p>'
		+
		'<p>I crafted you a backpack, where you\'ll find a <span class="is-highlight"><em>stick of wood</em></span> to defend yourself against thoses beasts.<br/><br/>And I also filled the backpack with some weird substances I found on the ground... Use them they help you, like some kind of <span class="is-highlight"><em>magical potions</em></span>.</p>'
		+
		'<br/>' + '<button class="nes-btn" onclick="introContinueTo11()">Continue</button>'
		)
	printItemsOwned(healthPotion)
	printItemsOwned(shieldPotion)
	printItemsOwned(staminaPotion)
	offDefaultPanelItems()
	printWeaponsOwned(woodstickWeapon)
	equipWoodstickWeapon()
	offDefaultPanelWeapons()
	autofocusMainAction()
}

// message #11 de la conversation d'introduction
function introContinueTo11() {
	msgInFeed(
		'<p class="title"><span class="is-hero">You</span></p>'
		+
		'<p>Uuuugh... ok dude I\'ll do it.</p>'
		+
		'<br/>' + '<button class="nes-btn" onclick="introContinueTo12()">Continue</button>'
		)
	autofocusMainAction()
}

// message #12 de la conversation d'introduction
function introContinueTo12() {
	msgInFeed(
		'<p class="title"><span class="is-old-man">Creepy old man</span></p>'
		+
		'<p>Great kid !<br/><br/>Take also this cycling helmet. I attached to it a whole set with : camera, micro, and speakers. I know this world and theses monsters, so I\'ll help you from here.</p>'
		+
		'<br/>' + '<button class="nes-btn" onclick="introContinueTo13()">Continue</button>'
		)
	autofocusMainAction()
}

// message #13 de la conversation d'introduction
function introContinueTo13() {
	msgInFeed(
		'<p class="title"><span class="is-hero">You</span></p>'
		+
		'<p>So why do you bother me then ? Go in by yourself ! FUCK !!</p>'
		+
		'<br/>' + '<button class="nes-btn" onclick="introContinueTo14()">Continue</button>'
		)
	autofocusMainAction()
}

// message #14 de la conversation d'introduction
function introContinueTo14() {
	msgInFeed(
		'<p class="title"><span class="is-old-man">Creepy old man</span></p>'
		+
		'<p><em>*Breathing calmly...*</em><br/><br/>Because I don\'t want, and you\'re my bitch. You understand !?</p>'
		+
		'<br/>' + '<button class="nes-btn" onclick="introContinueTo15()">Continue</button>'
		)
	autofocusMainAction()
}

// message #15 de la conversation d'introduction
function introContinueTo15() {
	msgInFeed(
		'<p>You are now facing the door of the building.</p>' + '<br/>' + '<button class="nes-btn" onclick="enterStage()">Open the door</button>'
		)
	autofocusMainAction()
}



/* END INTRODUCTION'S SPEECH  */

var roomsForStage = ["Chest", "Monster", "Chest", "Gate", "Monster", "Chest", "Monster", "Key", "Monster"]
var currentStage = 0
var currentRoom = 0
// '0' for no key, and so '1' for having the key
var hasKey = 0
// variable saying if the hero is currently in fighting or not (0: no, 1: yes)
var fighting = 0

// distribute randomly the rooms for the stage
function distributeRooms() {
	roomsForStage.sort(function(a, b){return 0.5 - Math.random()})
}

function openGate() {
	if (hasKey == 1) {
		msgInFeed(
			'<p>The <span class="is-highlight">Gate</span> is now open !</p>' + '<br/>' + '<button class="nes-btn" onclick="betweenStage()">Next stage</button>' + '<button class="nes-btn" onclick="endingRoom()">Stay here</button>'
			)
	} else {
		msgInFeed(
			'<p>You need <span class="is-highlight">Key</span> to open the Gate</p>' + '<br/>' + '<button class="nes-btn" onclick="endingRoom()">Ok</button>'
			)
		autofocusMainAction()
	}
}

function takeKey() {
	hasKey = 1
	endingRoom()
}

// randomly choose a monster according the current stage
function chooseMonster() {
	// Before stage 3, monsters have a level range between 1 and 2
	if (currentStage < 3) {
		currentMonster = existingMonsters[Math.floor(Math.random()*(existingMonsters.length-9))]
	} 
	// Between stage 3 and before 6, monsters have a level range between 1 and 4
	else if (currentStage >= 3 && currentStage < 6) {
		currentMonster = existingMonsters[Math.floor(Math.random()*(existingMonsters.length-7))]
	} 
	// Between stage 6 and before 9, monsters have a level range between 1 and 6
	else if (currentStage >= 6 && currentStage < 9) {
		currentMonster = existingMonsters[Math.floor(Math.random()*(existingMonsters.length-5))]
	} 
	// Between stage 9 and before 12, monsters have a level range between 1 and 8
	else if (currentStage >= 9 && currentStage < 12) {
		currentMonster = existingMonsters[Math.floor(Math.random()*(existingMonsters.length-3))]
	} 
	// Between stage 12 and before 15, monsters have a level range between 1 and 10
	else if (currentStage >= 12 && currentStage < 15) {
		currentMonster = existingMonsters[Math.floor(Math.random()*(existingMonsters.length-1))]
	} 
	// From stage 15, monsters can have any level (1 to 11)
	else {
		currentMonster = existingMonsters[Math.floor(Math.random()*existingMonsters.length)]
	}
}

function engageFight() {
	removeElements('msg')
	fighting = 1
	// get the monster	
	chooseMonster()

	// create the battle scene
	var msg = document.createElement('DIV')
	msg.className += "nes-container is-rounded is-dark msg"
	msg.id = "battle-scene"
	msg.innerHTML = 
	'<div class="nes-container is-rounded is-dark with-title">' + '<p class="title">Monster:' + currentMonster[0] + '</p>'
	+
	'<progress class="nes-progress is-primary" id="monster-stats" ' + 'value="' + currentMonster[2] + '" ' + 'min="0" ' + 'max="' + currentMonster[3] + '"' + '></progress>'
	+
	'</div>'
	+
	'<button class="nes-btn" onclick="triggerHeroAttack()">Attack</button>' + '<p class="nes-text is-error hide" id="alert-stamina"></p>'
	// put the battle scene in the feed
	document.getElementById("feed").appendChild(msg)
	document.getElementById('main').scrollTo(0,document.getElementById('main').scrollHeight)
}

function openChest() {
	// first determine if the loot is a item or a weapon
	var r = Math.floor(Math.random()*9)
	// if r is between 0 and 6 then the loot is an item, if it's 7 then a weapon, and else the chest is empty
	if (r >= 0 && r <= 5) {
		// determine wich item
		wichItemLoot = itemsExisting[Math.floor(Math.random()*itemsExisting.length)]
		// determine much item
		muchItemLoot = Math.floor(Math.random()*(2-1))+1
		// displaying the item's loot
		msgInFeed(
			'<p>You find ' + muchItemLoot + ' ' + wichItemLoot[0] + '.</p>' + '<button class="nes-btn" onclick="takeItemLoot(wichItemLoot)">Take the loot</button>'
			)
		autofocusMainAction()
	} else if (r == 7) {
		// determine the weapons lootable (those the hero still do not own, but existing)
		weaponsLootable = weaponsExisting.filter(function(n){return weaponsOwned.indexOf(n)>-1?false:n})
		// determine wich weapon between thoses lootable
		wichWeapon = weaponsLootable[Math.floor(Math.random()*weaponsLootable.length)]
		// displaying the weapon's loot
		msgInFeed(
			'<p>You find ' + wichWeapon[0] + '.</p>' + '<button class="nes-btn" onclick="takeWeaponLoot(wichWeapon)">Take the loot</button>'
			)
		autofocusMainAction()
	} else {
		// displaying that the chest is empty
		msgInFeed(
			'<p>The chest is empty.</p>' + '<br/>' + '<button class="nes-btn" onclick="endingRoom()">Ok</button>'
			)
		autofocusMainAction()
	}
}

function takeItemLoot(item) {
	if (itemsOwned.includes(item) == false) {
		itemsOwned.push(item)
		printItemsOwned(item)
	}
	item[2] += muchItemLoot
	refreshMuchItems()
	/* ici utiliser un switch avec comme argument l'item, et pour chaque cas décrire l'action */
	switch (item) {
		case healthPotion :
			// make the health potion usable again
			if (healthPotion[2] > 0) {
				// enabling the button
				document.getElementById('use-health-potion').disabled = false
				document.getElementById('use-health-potion').classList.remove('is-disabled')
				// retirer la couleur d'erreur
				document.getElementById('much-health-potion').parentNode.classList.remove('is-error')
			};
			break;
		case shieldPotion : 
			// make the shield potion usable again
			if (shieldPotion[2] > 0) {
				// enabling the button
				document.getElementById('use-shield-potion').disabled = false
				document.getElementById('use-shield-potion').classList.remove('is-disabled')
				// retirer la couleur d'erreur
				document.getElementById('much-shield-potion').parentNode.classList.remove('is-error')
			};
			break;
		case staminaPotion :
			// make the stamina potion usable again
			if (staminaPotion[2] > 0) {
				// enabling the button
				document.getElementById('use-stamina-potion').disabled = false
				document.getElementById('use-stamina-potion').classList.remove('is-disabled')
				// retirer la couleur d'erreur
				document.getElementById('much-stamina-potion').parentNode.classList.remove('is-error')
			};
			break;
		case boostPotion :
			// make the boost potion usuable again
			if (boostPotion[2] > 0) {
				// enabling the button
				document.getElementById('use-boost-potion').disabled = false
				document.getElementById('use-boost-potion').classList.remove('is-disabled')
				// retirer la couleur d'erreur
				document.getElementById('much-boost-potion').parentNode.classList.remove('is-error')
			};
			break;
		case energyDrain :
			// make the energy drain usuable again
			if (energyDrain[2] > 0) {
				// enabling the button
				document.getElementById('use-energy-drain').disabled = false
				document.getElementById('use-energy-drain').classList.remove('is-disabled')
				// retirer la couleur d'erreur
				document.getElementById('much-energy-drain').parentNode.classList.remove('is-error')
			};
			break;
	}
	lootTaken()
	endingRoom()
}

function takeWeaponLoot(weapon) {
	weaponsOwned.push(weapon)
	printWeaponsOwned(weapon)
	lootTaken()
	endingRoom()
}

function lootTaken() {
	roomsForStage[currentRoom] = "emptyChest"
}

/* START : MOVING BETWEEN ROOMS AND STAGES */

function enterStage() {
	// rewrite the value of 'roomsForStage' array to reset it, because it may be edited during the play in the previous floor 
	roomsForStage = ["Chest", "Monster", "Chest", "Gate", "Monster", "Chest", "Monster", "Key", "Monster"]
	// hide the top-right floor count
	document.getElementById('count-floor').classList.replace('show', 'hide')
	// display a mid-screen announcing the new current floor
	txtInFeed('Floor ' + currentStage)
	// after 1500ms hide mid-screen and enter in a room
	setTimeout(function() {
		distributeRooms()
		// placing the Hero in one of these rooms
		currentRoom = Math.floor(Math.random()*9)
		enterRoom()
	}, 1500)
	// update in front-end the current floor
	document.getElementById('value-count-floor').innerHTML = currentStage
	// display again the top-right floor count
	document.getElementById('count-floor').classList.replace('hide', 'show')
}

function enterRoom() {
	switch (roomsForStage[currentRoom]) {
		case "Gate" :
			msgInFeed(
				'<p>You find the <span class="is-highlight">Gate</span> to the next stage.</p>' + '<br/>' + '<button class="nes-btn" onclick="openGate()">Open the gate</button>'
				)
			autofocusMainAction()
			break;
		case "Key" :
			msgInFeed(
				'<p>You find the <span class="is-highlight">Key</span> for the gate to the next stage.</p>' + '<br/>' + '<button class="nes-btn" onclick="takeKey()">Get the key</button>'
				)
			autofocusMainAction()
			break;
		case "Chest" :
			msgInFeed(
				'<p>You find a <span class="is-highlight">Chest</span>.</p>' + '<br/>' + '<button class="nes-btn" onclick="openChest()">Open the chest</button>'
				)
			autofocusMainAction()
			break;
		case "emptyChest" :
			msgInFeed(
				'<p>A <span class="is-highlight">Chest</span> in wich you already take the loot.</p>' + '<br/>' + '<button class="nes-btn" onclick="endingRoom()">Ok</button>'
				)
			autofocusMainAction()
			break;
		case "Monster" :
			msgInFeed(
				'<p>You\'re now facing a <span class="is-highlight">Monster</span>, and his henchmans have close the doors.</p>' + '<br/>' + '<button class="nes-btn" onclick="engageFight()">Fight the monster</button>'
				)
			autofocusMainAction()
			break;
	}
}

function endingRoom() {
	// generic text after the room is cleared
	var clearedRoomText = '<p>Where to continue now ?</p>' + '<br/>'
	// listener to the directions buttons
	var btnGoNorth = 'onclick="goNorth()"'
	var btnGoEast = 'onclick="goEast()"'
	var btnGoSouth = 'onclick="goSouth()"'
	var btnGoWest = 'onclick="goWest()"'

	switch (currentRoom) {
		case 0 :
			msgInFeed(
				clearedRoomText + '<button class="nes-btn" ' + btnGoEast + '>East</button>' + '<button class="nes-btn" ' + btnGoSouth + '>South</button>'
				)
			autofocusMainAction()
			break;
		case 1 :
			msgInFeed(
				clearedRoomText + '<button class="nes-btn" ' + btnGoEast + '>East</button>' + '<button class="nes-btn" ' + btnGoSouth + '>South</button>' + '<button class="nes-btn" ' + btnGoWest + '>West</button>'
				)
			autofocusMainAction()
			break;
		case 2 :
			msgInFeed(
				clearedRoomText + '<button class="nes-btn" ' + btnGoSouth + '>South</button>' + '<button class="nes-btn" ' + btnGoWest + '>West</button>'
				)
			autofocusMainAction()
			break;
		case 3 :
			msgInFeed(
				clearedRoomText + '<button class="nes-btn" ' + btnGoNorth + '>North</button>' + '<button class="nes-btn" ' + btnGoEast + '>East</button>' + '<button class="nes-btn" ' + btnGoSouth + '>South</button>'
				)
			autofocusMainAction()
			break;
		case 4 :
			msgInFeed(
				clearedRoomText + '<button class="nes-btn" ' + btnGoNorth + '>North</button>' + '<button class="nes-btn" ' + btnGoEast + '>East</button>' + '<button class="nes-btn" ' + btnGoSouth + '>South</button>' + '<button class="nes-btn" ' + btnGoWest + '>West</button>'
				)
			autofocusMainAction()
			break;
		case 5 :
			msgInFeed(
				clearedRoomText + '<button class="nes-btn" ' + btnGoNorth + '>North</button>' + '<button class="nes-btn" ' + btnGoSouth + '>South</button>' + '<button class="nes-btn" ' + btnGoWest + '>West</button>'
				)
			autofocusMainAction()
			break;
		case 6 :
			msgInFeed(
				clearedRoomText + '<button class="nes-btn" ' + btnGoNorth + '>North</button>' + '<button class="nes-btn" ' + btnGoEast + '>East</button>'
				)
			autofocusMainAction()
			break;
		case 7 :
			msgInFeed(
				clearedRoomText + '<button class="nes-btn" ' + btnGoNorth + '>North</button>' + '<button class="nes-btn" ' + btnGoEast + '>East</button>' + '<button class="nes-btn" ' + btnGoWest + '>West</button>'
				)
			autofocusMainAction()
			break;
		case 8 :
			msgInFeed(
				clearedRoomText + '<button class="nes-btn" ' + btnGoNorth + '>North</button>' + '<button class="nes-btn" ' + btnGoWest + '>West</button>'
				)
			autofocusMainAction()
			break;
	}
}

function goNorth() {
	switch (currentRoom) {
		case 3 :
			currentRoom = 0
			break;
		case 4 :
			currentRoom = 1
			break;
		case 5 :
			currentRoom = 2
			break;
		case 6 :
			currentRoom = 3
			break;
		case 7 :
			currentRoom = 4
			break;
		case 8 :
			currentRoom = 5
			break;
	}
	enterRoom()
}

function goEast() {
	switch (currentRoom) {
		case 0 :
			currentRoom = 1
			break;
		case 1 :
			currentRoom = 2
			break;
		case 3 :
			currentRoom = 4
			break;
		case 4 :
			currentRoom = 5
			break;
		case 6 :
			currentRoom = 7
			break;
		case 7 :
			currentRoom = 8
			break;
	}
	enterRoom()
}

function goSouth() {
	switch (currentRoom) {
		case 0 :
			currentRoom = 3
			break;
		case 1 :
			currentRoom = 4
			break;
		case 2 :
			currentRoom = 5
			break;
		case 3 :
			currentRoom = 6
			break;
		case 4 :
			currentRoom = 7
			break;
		case 5 :
			currentRoom = 8
			break;
	}
	enterRoom()
}

function goWest() {
	switch (currentRoom) {
		case 1 :
			currentRoom = 0
			break;
		case 2 :
			currentRoom = 1
			break;
		case 4 :
			currentRoom = 3
			break;
		case 5 :
			currentRoom = 4
			break;
		case 7 :
			currentRoom = 6
			break;
		case 8 :
			currentRoom = 7
			break;
	}
	enterRoom()
}

function rest() {
	/*// restore health by 250% and restrain to max health points
	hero[0] = Math.round(hero[0] + (hero[0]*2.5))*/
	hero[0] = hero[0] + 300
	if (hero[0] > hero[1]) {
			hero[0] = hero[1]
	}
	/*// restore shield by 250% and restrain to max shield points
	hero[2] = Math.round(hero[2] + (hero[2]*2.5))*/
	hero[2] = hero[2] + 300
	if (hero[2] > hero[3]) {
			hero[2] = hero[3]
	}
	/*// restore stamina by 250% and restrain to max stamina points
	hero[4] = Math.round(hero[4] + (hero[4]*2.5))*/
	hero[4] = hero[4] + 300
	if (hero[4] > hero[5]) {
			hero[4] = hero[5]
	}
	// refresh hero's stats
	insertHeroValues()
	leaveStage()
}

function increaseStrength() {
	hero[7] = hero[7]+0.2
	insertHeroValues()
	leaveStage()
}

function increaseDrugology() {
	hero[8] = hero[8]+0.2
	insertHeroValues()
	leaveStage()
}

var optionsBetweenStage = '<br/><br/><ul class="optionsBetweenStage-list"><li><button class="nes-btn" onclick="rest()">Rest</button></li><li><button class="nes-btn" onclick="increaseStrength()">Get stronger</button></li><li><button class="nes-btn" onclick="increaseDrugology()">Better drug</button></li><li><button class="nes-btn" onclick="leaveStage()">Do nothing</button></li></ul>'

function betweenStage() {
	// display a mid-screen announcing the new current floor
	txtInFeed('Before enter to the next stage : ' + optionsBetweenStage)
}

function leaveStage() {
	currentStage++
	hasKey = 0
	enterStage()
}

function restartGame() {
	location.reload(true)
}

/* END : MOVING BETWEEN ROOMS AND STAGES */

/* START FOR OTHERS FUNCTIONS */

function txtInFeed(content) {
	// delete others txt and msg in the feed
	removeElements('txt')
	removeElements('msg')
	// create a new txt in the feed
	var txt = document.createElement('P')
	txt.className += "nes-text txt"
	txt.innerHTML =	content
	document.getElementById("feed").appendChild(txt)
}

function msgInFeed(content) {
	// delete others txt and msg in the feed
	removeElements('txt')
	removeElements('msg')
	// create a new msg in the feed
	var msg = document.createElement('DIV')
	msg.className += "nes-container is-rounded is-dark with-title msg"
	msg.innerHTML =	content
	document.getElementById("feed").appendChild(msg)
}

function removeElements(className) {
	var elements = document.getElementsByClassName(className)
	while(elements.length > 0) {
		elements[0].parentNode.removeChild(elements[0])
	}
}

function autofocusMainAction() {
	var n = document.querySelectorAll('#feed .nes-btn')
	if (n.length == 1) {
		n[0].focus()
	}
}

/* END FOR OTHERS FUNCTIONS */
